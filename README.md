## Requisitos para rodar a API

- Tenha o docker instalado na sua máquinae o Insomia

## Começando

- Clone o projeto do repositório
```git
  git clone
```

**Dentro do projeto**

- Renomei o arquivo .env.example para .env


**No terminal, dentro do projeto**
```docker
  docker-compose build
```

```docker
  docker-compose up
```
**Rodas as migrations**
```docker
  docker-compose exec api yarn typeorm migration:run
```

**Rodar os testes**
```docker
  docker-compose exec api yarn test
```

**Importe o arquivo [insomnia.json](./insomnia.json) no Insomia**


**Usando a API**

  Ao importar o json no insomia, ele virá com os métodos e a forma para testar em cada um dos enpoints

A instalação foi realizar nos sistemas operacionais Ubuntu e Arch

Feito com :heart: por Luann Rodrigo
