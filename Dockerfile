FROM node:14 as base

WORKDIR /usr/app

COPY package*.json ./

RUN yarn install

COPY . .

CMD ["yarn", "run", "dev:server"]
