import { Router } from 'express';

import usersRouter from '@modules/users/infra/http/routes/users.routes';
import sessionsRouter from '@modules/users/infra/http/routes/sessions.routes';
import passwordRouter from '@modules/users/infra/http/routes/password.routes';
import profileRouter from '@modules/users/infra/http/routes/profile.routes';
import listsRouter from '@modules/lists/infra/http/routes/lists.routes';
import productsRouter from '@modules/products/infra/http/routes/products.routes';
import favoritiesRouter from '@modules/favorities/infra/http/routes/favorities.routes';

const routes = Router();

routes.use('/users', usersRouter);
routes.use('/sessions', sessionsRouter);
routes.use('/password', passwordRouter);
routes.use('/profile', profileRouter);
routes.use('/lists', listsRouter);
routes.use('/products', productsRouter);
routes.use('/favorities', favoritiesRouter);

export default routes;
