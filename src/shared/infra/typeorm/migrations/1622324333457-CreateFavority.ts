import {MigrationInterface, QueryRunner, Table} from "typeorm";

export default class CreateFavority1622324333457 implements MigrationInterface {

public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
          new Table({
            name: 'favorities',
            columns: [
              {
                name: 'id',
                type: 'uuid',
                isPrimary: true,
                generationStrategy: 'uuid',
                default: 'uuid_generate_v4()',
              },
              {
                name: 'product_id',
                type: 'uuid'
              },
              {
                name: 'list_id',
                type: 'uuid'
              },
              {
                name: 'created_at',
                type: 'timestamp',
                default: 'now()'
              },
              {
                name: 'updated_at',
                type: 'timestamp',
                default: 'now()'
              }
            ],
            foreignKeys: [
              {
                name: 'FavoritiesList',
                referencedTableName: 'lists',
                referencedColumnNames: ['id'],
                columnNames: ['list_id'],
                onDelete: 'CASCADE',
                onUpdate: 'CASCADE'
              }
            ]
          })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropTable('favorities');
    }

}
