import { container } from 'tsyringe';

import '@modules/users/providers';
import './providers';

import IFavoritiesRepository from '@modules/favorities/repositories/IFavoritiesRepository';
import FavoritiesRepository from '@modules/favorities/infra/typeorm/repositories/FavoritiesRepository'

import IListsRepository from '@modules/lists/repositories/IListsRepository';
import ListsRepository from '@modules/lists/infra/typeorm/repositories/ListsRepository';


import IUsersRepository from '@modules/users/repositories/IUsersRepository';
import UsersRepository from '@modules/users/infra/typeorm/repositories/UsersRepository';

import IUserTokensRepository from '@modules/users/repositories/IUserTokensRepository';
import UserTokensRepository from '@modules/users/infra/typeorm/repositories/UserTokensRepository';

container.registerSingleton<IFavoritiesRepository>(
  'FavoritiesRepository',
  FavoritiesRepository,
);

container.registerSingleton<IListsRepository>(
  'ListsRepository',
  ListsRepository,
);

container.registerSingleton<IUsersRepository>(
  'UsersRepository',
  UsersRepository,
);

container.registerSingleton<IUserTokensRepository>(
  'UserTokensRepository',
  UserTokensRepository,
);
