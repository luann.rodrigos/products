import { v4 as uuid } from 'uuid';

import ICreateListsDTO from '@modules/lists/dtos/ICreateListsDTO';
import IFindAllListsDTO from '@modules/lists/dtos/IFindAllListsDTO';
import IListsRepository from '@modules/lists/repositories/IListsRepository';
import List from '@modules/lists/infra/typeorm/entities/List';

class FakeListsRepository implements IListsRepository {
  private lists: List[] = [];

  public async create({ name, provider_id }: ICreateListsDTO): Promise<List> {
    const createList = new List();

    Object.assign(createList, { id: uuid(), name, provider_id });

    this.lists.push(createList);

    return createList;
  }

  public async save(list: List): Promise<List> {
    const findTeamsByIndex = this.lists.findIndex(findList => findList.id === list.id);

    this.lists[findTeamsByIndex] = list;

    return list;
  }

  public async findAllLists({ user_id }: IFindAllListsDTO): Promise<List[]> {
    const findListByUserId = this.lists.filter(list => list.provider_id === user_id);

    return findListByUserId;
  }
}

export default FakeListsRepository;
