import List from '@modules/lists/infra/typeorm/entities/List';
import ICreateListsDTO from '@modules/lists/dtos/ICreateListsDTO';
import IFindAllListsDTO from '@modules/lists/dtos/IFindAllListsDTO';

export default interface IUsersRepository {
  create(data: ICreateListsDTO): Promise<List>;
  findAllLists(data: IFindAllListsDTO): Promise<List[]>;
}
