import { Router } from 'express';

import ListsController from '@modules/lists/infra/controllers/ListsController';
import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';

const listsRoute = Router();

listsRoute.use(ensureAuthenticated);

const listsController = new ListsController();

listsRoute.post('/', listsController.create)
listsRoute.get('/', listsController.index)

export default listsRoute;
