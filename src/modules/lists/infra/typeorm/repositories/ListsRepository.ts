import { getRepository, Repository } from 'typeorm';

import IListsRepository from '../../../repositories/IListsRepository';
import ICreateListsDTO from '../../../dtos/ICreateListsDTO';

import List from '../entities/List';
import IFindAllListsDTO from '@modules/lists/dtos/IFindAllListsDTO';

class ListsRepository implements IListsRepository {
  private ormRepository: Repository<List>;

  constructor() {
    this.ormRepository = getRepository(List);
  }

  public async create({ name, provider_id }: ICreateListsDTO): Promise<List> {
    const list = this.ormRepository.create({
      name,
      provider_id
    });

    await this.ormRepository.save(list);

    return list;
  }

  public async findAllLists({ user_id }: IFindAllListsDTO): Promise<List[]> {
    const lists = this.ormRepository.find({
      where: {
        provider_id: user_id
      }
    });

    return lists;
  }
}
export default ListsRepository;
