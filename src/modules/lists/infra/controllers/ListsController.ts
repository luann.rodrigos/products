import { Request, Response } from 'express';
import { container } from 'tsyringe';

import CreateListsService from '@modules/lists/services/CreateListsService'
import ListAllListsService from '@modules/lists/services/ListAllListsService'

export default class ListsController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { name } = request.body;
    const provider_id = request.user.id;

    const createListsService = container.resolve(CreateListsService);

    const list = await createListsService.execute({ name, provider_id })

    return response.json(list);
  }

  public async index(request: Request, response: Response): Promise<Response> {
    const user_id = request.user.id;

    const listAllListsService = container.resolve(ListAllListsService);

    const lists = await listAllListsService.execute({ user_id });

    return response.json(lists)

  }
}
