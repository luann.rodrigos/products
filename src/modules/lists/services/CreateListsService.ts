import { injectable, inject } from 'tsyringe';

import List from '@modules/lists/infra/typeorm/entities/List';
import IListsRepository from '@modules/lists/repositories/IListsRepository';
import AppError from '@shared/errors/AppError';

interface IRequestDTO {
  name: string;
  provider_id: string;
}

@injectable()
class CreateListsService {
  constructor(
    @inject('ListsRepository')
    private listsRepository: IListsRepository,
  ) {}

  public async execute({ name, provider_id }: IRequestDTO): Promise<List> {
    const countList = await this.listsRepository.findAllLists({ user_id: provider_id });

    if (countList.length > 0) {
      throw new AppError("Provider cannot create list");
    }

    const lists = await this.listsRepository.create({ name, provider_id });

    return lists;
  }

}

export default CreateListsService;
