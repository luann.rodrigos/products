import AppError from '@shared/errors/AppError';

import FakeListsRepository from '../repositories/fakes/FakeListsRepository';
import CreateListsService from './CreateListsService';

let fakeListsRepository: FakeListsRepository;
let createListsService: CreateListsService;

describe('CreateUserService', () => {
  beforeEach(() => {
    fakeListsRepository = new FakeListsRepository();
    createListsService = new CreateListsService(
      fakeListsRepository
    )
  });

  it('shold be able to create list', async () => {
    const list = await createListsService.execute({
      name: 'Dev Team',
      provider_id: 'bb4ab275-db7e-4ced-82b1-9222a36de5d1'
    });

    expect(list).toHaveProperty('id');
  });

  it('shold be not able to create two list', async () => {
    const list = await createListsService.execute({
      name: 'Dev Team',
      provider_id: 'bb4ab275-db7e-4ced-82b1-9222a36de5d1'
    });

    await expect(
      createListsService.execute({
        name: 'Dev Team',
        provider_id: 'bb4ab275-db7e-4ced-82b1-9222a36de5d1'
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

})
