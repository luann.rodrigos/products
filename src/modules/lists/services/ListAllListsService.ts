import { injectable, inject } from 'tsyringe';

import Teams from '@modules/lists/infra/typeorm/entities/List';
import IListsRepository from '@modules/lists/repositories/IListsRepository';

interface IRequestDTO {
  user_id: string;
}

@injectable()
class ListAllListsService {
  constructor(
    @inject('ListsRepository')
    private listsRepository: IListsRepository,
  ) {}

  public async execute({ user_id }: IRequestDTO): Promise<Teams[]> {
    const lists = this.listsRepository.findAllLists({ user_id });

    return lists;
  }

}

export default ListAllListsService;
