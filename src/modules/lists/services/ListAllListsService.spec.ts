import AppError from '@shared/errors/AppError';

import FakeListsRepository from '../repositories/fakes/FakeListsRepository';
import ListAllListsService from './ListAllListsService';

let fakeListsRepository: FakeListsRepository;
let listAllListsService: ListAllListsService;

describe('CreateUserService', () => {
  beforeEach(() => {
    fakeListsRepository = new FakeListsRepository();
    listAllListsService = new ListAllListsService(
      fakeListsRepository
    )
  });

  it('shold be able to list all provider s teams ', async () => {
    const listOne = await fakeListsRepository.create({
      name: 'Dev Team',
      provider_id: 'bb4ab275-db7e-4ced-82b1-9222a36de5d1'
    });

    const listTwo = await fakeListsRepository.create({
      name: 'Dev Team',
      provider_id: 'bb4ab275-db7e-4ced-82b1-9222a36de5d1'
    });

    const listThree = await fakeListsRepository.create({
      name: 'Dev Team',
      provider_id: 'bb4ab275-db7e-4ced-82b1-9222a36de5d1'
    });

    const listQua = await fakeListsRepository.create({
      name: 'Dev Team',
      provider_id: '7fa02ade-1a58-4b83-abf1-34784d9350f6'
    });

    const list = await listAllListsService.execute({ user_id: "bb4ab275-db7e-4ced-82b1-9222a36de5d1" })


    expect(list).toEqual([listOne, listTwo, listThree])
  });
})
