import { injectable, inject } from 'tsyringe';

import Favority from '@modules/favorities/infra/typeorm/entities/Favority';
import IFavoritiesRepository from '@modules/favorities/repositories/IFavoritiesRepository';
import AppError from '@shared/errors/AppError';

interface IRequestDTO {
  product_id: string;
  list_id: string;
}

@injectable()
class AddFavorityService {
  constructor(
    @inject('FavoritiesRepository')
    private favoritiesRepository: IFavoritiesRepository,
  ) {}

  public async execute({ product_id, list_id }: IRequestDTO): Promise<Favority> {
    const favority = await this.favoritiesRepository.findByProductId({ product_id, list_id });

    if (Array.isArray(favority) && favority.length) {
      throw new AppError("Product already added to this list");
    }

    const lists = await this.favoritiesRepository.create({ product_id, list_id });

    return lists;
  }

}

export default AddFavorityService;
