import AppError from '@shared/errors/AppError';

import FakeListsRepository from '../repositories/fakes/FakeFavorityRepository';
import CreateListsService from './AddFavorityService';

let fakeListsRepository: FakeListsRepository;
let createListsService: CreateListsService;

describe('CreateUserService', () => {
  beforeEach(() => {
    fakeListsRepository = new FakeListsRepository();
    createListsService = new CreateListsService(
      fakeListsRepository
    )
  });

  it('shold be able to add products ', async () => {
    const fav = await createListsService.execute({
      product_id: '5500416d-8853-406f-a604-ed83fa9f95e9',
      list_id: 'bb4ab275-db7e-4ced-82b1-9222a36de5d1'
    });

    expect(fav).toHaveProperty('id');
  });

  it('shold be not able add same product twice', async () => {
    const fav = await createListsService.execute({
      product_id: '5500416d-8853-406f-a604-ed83fa9f95e9',
      list_id: 'bb4ab275-db7e-4ced-82b1-9222a36de5d1'
    });

    await expect(
      createListsService.execute({
        product_id: '5500416d-8853-406f-a604-ed83fa9f95e9',
        list_id: 'bb4ab275-db7e-4ced-82b1-9222a36de5d1',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

})
