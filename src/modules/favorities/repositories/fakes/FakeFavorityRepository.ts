import { v4 as uuid } from 'uuid';

import IAddFavorityDTO from '@modules/favorities/dtos/IAddFavorityDTO';
import IFindProductDTO from '@modules/favorities/dtos/IFindProductDTO';
import IFavoritiesRepository from '@modules/favorities/repositories/IFavoritiesRepository';
import Favority from '@modules/favorities/infra/typeorm/entities/Favority';

class FakeFavorityRepository implements IFavoritiesRepository {
  private favorities: Favority[] = [];

  public async create({ product_id, list_id }: IAddFavorityDTO): Promise<Favority> {
    const listFavority = new Favority();

    Object.assign(listFavority, { id: uuid(), product_id, list_id });

    this.favorities.push(listFavority);

    return listFavority;
  }

  public async save(favority: Favority): Promise<Favority> {
    const findFavorityByIndex = this.favorities.findIndex(findFav => findFav.id === favority.id);

    this.favorities[findFavorityByIndex] = favority;

    return favority;
  }

  public async findByProductId({ product_id }: IFindProductDTO): Promise<Favority[]> {
    const findByProductId = this.favorities.filter(favority => favority.product_id === product_id);

    return findByProductId;
  }
}

export default FakeFavorityRepository;
