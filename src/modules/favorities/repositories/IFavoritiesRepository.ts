import Favority from '@modules/favorities/infra/typeorm/entities/Favority';
import IAddFavorityDTO from '@modules/favorities/dtos/IAddFavorityDTO';

export default interface IUsersRepository {
  create(data: IAddFavorityDTO): Promise<Favority>;
  findByProductId({ product_id, list_id }: IAddFavorityDTO): Promise<Favority[] | undefined>;
  // findAllLists(data: IFindAllListsDTO): Promise<List[]>;
}
