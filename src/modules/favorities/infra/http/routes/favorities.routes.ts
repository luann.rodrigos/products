import { Router } from 'express';

import FavoritiesController from '@modules/favorities/infra/controllers/FavoritiesController';
import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';

const favoritiesRoute = Router();

favoritiesRoute.use(ensureAuthenticated);

const favoritiesController = new FavoritiesController();

favoritiesRoute.post('/', favoritiesController.create)

export default favoritiesRoute;
