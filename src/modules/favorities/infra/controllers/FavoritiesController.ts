import { Request, Response } from 'express';
import { container } from 'tsyringe';

import AddFavorityService from '@modules/favorities/services/AddFavorityService'
import ListAllListsService from '@modules/lists/services/ListAllListsService'
import api from '@externals/api';
import AppError from '@shared/errors/AppError';

export default class FavoritiesController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { product_id, list_id } = request.body;

    try {
      const { data } = await api.get(`${product_id}/`);

      const addFavorityService = container.resolve(AddFavorityService);

      const favority = await addFavorityService.execute({ product_id, list_id })

      return response.json(favority);

    } catch (error) {
      throw new AppError("Product not exist or duplicated, please verify list products or contact administrator");
    }
  }

  public async index(request: Request, response: Response): Promise<Response> {
    const user_id = request.user.id;

    const listAllListsService = container.resolve(ListAllListsService);

    const lists = await listAllListsService.execute({ user_id });

    return response.json(lists)

  }
}
