import { getRepository, Repository } from 'typeorm';

import IFavoritiesRepository from '../../../repositories/IFavoritiesRepository';
import IAddFavorityDTO from '../../../dtos/IAddFavorityDTO';

import Favority from '../entities/Favority';
// import IFindAllListsDTO from '@modules/lists/dtos/IFindAllListsDTO';

class FavoritiesRepository implements IFavoritiesRepository {
  private ormRepository: Repository<Favority>;

  constructor() {
    this.ormRepository = getRepository(Favority);
  }

  public async create({ product_id, list_id }: IAddFavorityDTO): Promise<Favority> {
    const favority = this.ormRepository.create({
      product_id,
      list_id
    });

    await this.ormRepository.save(favority);

    return favority;
  }

  public async findByProductId({ product_id, list_id }: IAddFavorityDTO): Promise<Favority[] | undefined> {
    const favority = await this.ormRepository.find({
      where: {
        product_id,
        list_id
      }
    });

    return favority;
  }

  // public async findAllLists({ user_id }: IFindAllListsDTO): Promise<List[]> {
  //   const lists = this.ormRepository.find({
  //     where: {
  //       provider_id: user_id
  //     }
  //   });

  //   return lists;
  // }
}
export default FavoritiesRepository;
