import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import List from '@modules/lists/infra/typeorm/entities/List';

@Entity('favorities')
class Favorities {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  product_id: string;

  @Column()
  list_id: string;

  @ManyToOne(() => List)
  @JoinColumn({ name: 'list_id' })
  provider: List;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Favorities;
