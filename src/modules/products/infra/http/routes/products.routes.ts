import { Router } from 'express';

import ProductsController from '@modules/products/infra/controllers/ProductsController';

const productsRoute = Router();

const productsController = new ProductsController();

productsRoute.get('/', productsController.index)
productsRoute.get('/:id', productsController.show)

export default productsRoute;
