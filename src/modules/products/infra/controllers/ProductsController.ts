import { Request, Response } from 'express';
import { container } from 'tsyringe';

import api from '@externals/api'

import AppError from '@shared/errors/AppError';



export default class ProductsController {
  public async index(request: Request, response: Response): Promise<Response> {
    const { page }: any  = request.query;
    const pagination = parseInt(page);

    try {
      const { data } = await api.get(`/?page=${pagination}`);;

      return response.json(data.products);

    } catch (error) {
      throw new AppError("Error with extenal api, verify with administrador");
    }
  }

    public async show(request: Request, response: Response): Promise<Response> {
    const { id }: any  = request.params;

    try {
      const { data } = await api.get(`/${id}/`);;

      return response.json(data);

    } catch (error) {
      throw new AppError("Error with extenal api, verify with administrador");
    }
  }
}
