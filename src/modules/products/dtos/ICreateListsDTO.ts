export default interface ICreateListsDTO {
  name: string;
  provider_id: string;
}
