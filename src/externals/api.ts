import axios from "axios";

// Pode ser algum servidor executando localmente:
// http://localhost:3000

const api = axios.create({
  baseURL: "http://challenge-api.luizalabs.com/api/product/",
});

export default api;
